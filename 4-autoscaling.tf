resource "aws_launch_configuration" "s1-instance-config" {
    name = "s1-launch-config"
    image_id = var.image_id
    instance_type = var.instance_type
    security_groups = [ aws_security_group.s1-asg.id ]
    key_name = var.key_pair
    associate_public_ip_address = true

    lifecycle {
        create_before_destroy = true
    }

    # To download content from S3 bucket
    user_data = <<-EOF
                #!/bin/bash
                sudo yum update -y
                sudo amazon-linux-extras install nginx1 -y
                sudo systemctl start nginx
                sudo chmod 777 ${var.web_hosting_location}
                aws configure set region ${var.region}
                export AWS_ACCESS_KEY_ID=${var.s3_user_access_key}
                export AWS_SECRET_ACCESS_KEY=${var.s3_user_secret_key}
                aws s3 sync s3://${var.s3_bucket} ${var.web_hosting_location}
                EOF
}

resource "aws_autoscaling_group" "s1-auto-scaling-group" {
    name = "s1-auto-scaling-group"
    min_size = 1
    max_size = 3
    desired_capacity = 3
    vpc_zone_identifier = [ aws_subnet.s1-private-subnet.id ]
    health_check_type = "ELB"
    
    launch_configuration = aws_launch_configuration.s1-instance-config.id
}

resource "aws_autoscaling_attachment" "s1-autoscaling-attachment" {
    autoscaling_group_name = aws_autoscaling_group.s1-auto-scaling-group.id
    lb_target_group_arn = aws_lb_target_group.s1-lb-target-group.arn
}