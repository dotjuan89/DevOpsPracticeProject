resource "aws_lb_target_group" "s1-lb-target-group" {
    name = "s1-lb-target-group"
    port = 80
    protocol = "HTTP"
    target_type = "instance"
    vpc_id = aws_default_vpc.default.id

    health_check {
        enabled = true
        interval = 10
        path = "/health"
        protocol = "HTTP"
        timeout = 5
        healthy_threshold = 3
        unhealthy_threshold = 3
    }
}

resource "aws_lb" "s1-lb" {
    name = "s1-lb"
    internal = false
    load_balancer_type = "application"
    security_groups = [aws_security_group.s1-lb.id]
    subnets = [ aws_default_subnet.s1-default-subnet-1.id, aws_default_subnet.s1-default-subnet-2.id ]

    enable_deletion_protection = false

    tags = {
        "Name" = "S1 Application Load Balancer"
    }
}

resource "aws_lb_listener" "s1-alb-listener" {
    load_balancer_arn = aws_lb.s1-lb.arn
    port = "80"
    protocol = "HTTP"

    default_action {
        type = "forward"
        target_group_arn = aws_lb_target_group.s1-lb-target-group.arn
    }
}