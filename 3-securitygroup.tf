resource "aws_security_group" "s1-lb" {
    name = "Allow HTTP for ALB"
    description = "Allow inbound HTTP traffic to ALB"
    vpc_id = aws_default_vpc.default.id

    ingress = [ {
        description = "HTTP Traffic"
        prefix_list_ids = []
        security_groups = []
        from_port = 80
        to_port = 80
        protocol = "tcp"
        cidr_blocks = [ "0.0.0.0/0" ]
        ipv6_cidr_blocks = [ "::/0" ]
        self = false
    } ]
    
    egress = [ {
        description = "Default Egress"
        prefix_list_ids = []
        security_groups = []
        from_port = 0
        to_port = 0
        protocol = "-1"
        cidr_blocks = [ "0.0.0.0/0" ]
        ipv6_cidr_blocks = [ "::/0" ]
        self = false
    } ]
}

resource "aws_security_group" "s1-asg" {
    name = "Allow HTTP traffic from ALB"
    description = "Allow inbound traffic from ALB"
    vpc_id = aws_default_vpc.default.id

    ingress = [ {
        description = "HTTP Traffic"
        prefix_list_ids = []
        security_groups = []
        from_port = 80
        to_port = 80
        protocol = "tcp"
        cidr_blocks = [ "0.0.0.0/0" ]
        ipv6_cidr_blocks = [ "::/0" ]
        self = false
    } ]
    

    egress = [ {
        description = "Default Egress"
        prefix_list_ids = []
        security_groups = []
        from_port = 0
        to_port = 0
        protocol = "-1"
        cidr_blocks = [ "0.0.0.0/0" ]
        ipv6_cidr_blocks = [ "::/0" ]
        self = false
    } ]
}

resource "aws_security_group_rule" "s1-security-group-rule" {
    type = "ingress"
    from_port = 80
    to_port = 80
    protocol = "tcp"
    security_group_id = aws_security_group.s1-asg.id
    source_security_group_id = aws_security_group.s1-lb.id
}