import os
import sys
import boto3 as b3

html_dir = "html"

session = b3.Session(
    aws_access_key_id=sys.argv[1],
    aws_secret_access_key=sys.argv[2]
)

client = session.client('s3')
for filename in os.listdir(html_dir):
    filepath = os.path.join(html_dir,  filename)
    if os.path.isfile(filepath):
        print("Converting {0} to uppercase...".format(filepath))
        with open(filepath, 'r+') as f:
            content = f.read().upper()
            response = client.put_object(
                Bucket=sys.argv[3],
                Body=content,
                Key=filename
            )


