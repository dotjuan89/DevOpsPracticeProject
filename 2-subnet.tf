resource "aws_default_subnet" "s1-default-subnet-1" {
    availability_zone = var.availability_zone_1

    tags = {
      "Name" = "S1 Default Subnet for ${var.availability_zone_1}"
    }
}

resource "aws_default_subnet" "s1-default-subnet-2" {
    availability_zone = var.availability_zone_2

    tags = {
      "Name" = "S1 Default Subnet for ${var.availability_zone_2}"
    }
}

resource "aws_subnet" "s1-private-subnet" {
    vpc_id = aws_default_vpc.default.id
    cidr_block = var.default_cidr_block
    availability_zone = var.availability_zone_1

    tags = {
        "Name" = "S1 Private Subnet"
    }
}